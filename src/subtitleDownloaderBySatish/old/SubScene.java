package subtitleDownloaderBySatish.old;


import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import subtitleDownloaderBySatish.old.Main.SearchResults;

class SubScene implements ParticularWebsite 
{

	static Main.searchText searchText;
	String movieName;
	public String getSelectedMovieName(){return movieName;};
	public void setSelectedMovieName(String x){movieName = x;}
	
	public Main.searchText getSearchTextObj(){
		return searchText;
	}
	
	public SubScene(String searchString)
	{
		this.searchText = new Main.searchText(searchString);
	}
	
	public void searchSub() throws Exception
	{
		searchText.searchResults= new HashMap<String, Main.SearchResults>();
		HashMap<String, Main.SearchResults> matches = (HashMap<String, Main.SearchResults>) Downloader.readFromFile(searchText.searchText);
		if(matches!=null){
			searchText.searchResults = matches;
		}

		//String all = "<a href=\"/subtitles/iron-man\">Iron Man (2008)</a>	</div>	<div class=\"subtle\">		303 subtitles	</div></li><li>	<div class=\"title\">		<a href=\"/subtitles/iron-man-and-hulk-heroes-united\">Iron Man &amp; Hulk: Heroes United (2013)</a>";
		String searchStringURL = "https://subscene.com/subtitles/title?q=".concat(searchText.searchText);
		System.out.println("Inside search Sub");
		System.out.println(searchStringURL);
		System.out.println("before");
		String all = Downloader.getStringFromURL(searchStringURL);
		System.out.println("after");
		System.out.println(all);
		
		Pattern p = Pattern.compile("<a href=\"(/sub.*?)\">(.*?)</a>");
		
		Matcher m = p.matcher(all);
		matches = new HashMap<String, Main.SearchResults>();
		while(m.find()){
			
			Main.SearchResults x = new Main.SearchResults(m.group(2),"https://subscene.com".concat(m.group(1)));
			matches.put(m.group(2), x);
		}
		searchText.searchResults = matches;
		//Downloader.saveToFile(matches,SubScene.searchText.searchText);
		
	}
	
	
	public void mainSub(String url) throws Exception {
		
		LinkedList<Main.subDetails> extractedList = HelperSubScene.extractListFromPage(url);
		

		System.out.println("===================");
		LinkedList<Thread> threadList = new LinkedList<Thread>();

		for (int i = 0; i < extractedList.size(); i++)
			threadList.add(HelperSubScene.sthread(extractedList.get(i)));

		for (int i = 0; i < threadList.size(); i++)
			threadList.get(i).join();
		
		System.out.println("Total "+extractedList.size()+" subtitles are ready to be downloaded");
		Downloader.downloadAllSubs(extractedList);
	}
}