package subtitleDownloaderBySatish.old;


import java.net.ConnectException;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


class HelperSubScene
{
	
	
	static LinkedList<Main.subDetails> extractListFromPage( String url) throws Exception
	{
		String all = Downloader.getStringFromURL(url);
//        Pattern p = Pattern.compile("<a href=\"(/sub.*?)\">.*?<span>.*?</span>.*?</a>");
		
        Pattern p = Pattern.compile("<a href=\"(/sub.*?)\">.*?<span.*?span>.*?<span>(.*?)</span>.*?</a>");
		
		Matcher m = p.matcher(all);
		LinkedList<Main.subDetails> matches = new LinkedList<Main.subDetails>();
		System.out.println("**********************************************");
		while(m.find()){
			Main.subDetails s= new Main.subDetails();
			s.link = "https://subscene.com".concat(m.group(1));
			s.name = m.group(2).trim();
		    matches.add( s);
//		    System.out.println(m.group(0));
//		    System.out.println(s.link);
//		    System.out.println(s.name);
		}
		
		System.out.println(matches.size()+" total subtitles found");

		for (Iterator<Main.subDetails> iterator = matches.iterator(); iterator.hasNext();) {
		    String string = iterator.next().link;
		    if (!string.contains("/english/")) {
		        iterator.remove();
		    }
		}
		
		System.out.println(matches.size()+" english subtitles found");
		
		HashSet<Main.subDetails> s = new HashSet<Main.subDetails>(matches);
		
		System.out.println(s.size()+" after removing duplicates");
		matches = new LinkedList<Main.subDetails>(s);
		
		return matches;
		
		
		}

	static void doThings(Main.subDetails temp) throws Exception
	{
		
		String subLink = Downloader.getStringFromURL(temp.link);
		
		
		int start = subLink.indexOf("/subtitle/download?mac=");
		
		if(start <0) return;
		int end = subLink.indexOf("\"",start+1);
		subLink = subLink.substring(start,end);
		
		temp.link2 = "https://subscene.com".concat(subLink);
		//System.out.println(subLink);
	}

	/*<a href="/subtitles/captive/portuguese/730125">					<span class="l r neutral-icon">						Portuguese					</span>			
	 * 		<span>						Captive.2012.FRENCH.DVDRip.XviD-UTT.PORT.[blueindigo] 					</span>				*/
	
	
	
    static Thread sthread(final Main.subDetails x)
    {
    	Thread t= new Thread(new Runnable(){

			public void run() {
				int count =0;
				while (true) {
					try {
						doThings(x);
						x.downloadLinkGenerated = true;
						break;
					} catch (ConnectException e) {
						System.out.println("Connection exception->" + x.link);
						if(count++ >2) break;
					} catch (Exception e) {
						if(count++ >2) break;
						System.out
								.println("Failed to fetch link for " + x.link);
						e.printStackTrace();
					}
				}
				
			}});
    	Downloader.executor.execute(t);
//    	t.start();
		
    	return t;
    }
    
}