package subtitleDownloaderBySatish.old;


import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import subtitleDownloaderBySatish.old.Main.searchText;

class OpenSub implements ParticularWebsite 
{

	String selectedMovieName;
	String searchString;
	ArrayList<Main.subDetails> searchResults;
	
	public String getSelectedMovieName(){return selectedMovieName;};
	public void setSelectedMovieName(String x){selectedMovieName = x;}

	public void setSearchResults(ArrayList<Main.subDetails> s){this.searchResults = s;};
	public ArrayList<Main.subDetails> getSearchResults(){return searchResults;};


	public OpenSub(String searchString)
	{
		this.searchString = searchString;
	}
	
	public searchText getSearchTextObj() {
		return null;
	}
	
	public void searchSub() throws Exception
	{
		searchString = searchString.replaceAll(" ", "+");
		String url = "http://www.opensubtitles.org/libs/suggest.php?format=json3&MovieName=".concat(searchString).concat("&SubLanguageID=eng");
		String all = Downloader.getStringFromURL(url);
		
		//String all ="[{\"name\":\"Vampire Diaries\",\"year\":\"2009\",\"total\":\"708\",\"id\":50810,\"pic\":\"1405406\"},{\"name\":\"Vampire Academy\",\"year\":\"2014\",\"total\":\"7\",\"id\":167610,\"pic\":\"1686821\"},{\"name\":\"Vampire's Kiss\",\"year\":\"1988\",\"total\":\"5\",\"id\":9015,\"pic\":\"98577\"},{\"name\":\"Vampire Hunter D: Bloodlust\",\"year\":\"2000\",\"total\":\"8\",\"id\":489,\"pic\":\"216651\"},{\"name\":\"Vampires Suck\",\"year\":\"2010\",\"total\":\"7\",\"id\":59198,\"pic\":\"1666186\"}]";
		
		System.out.println(all);
		
		Pattern p = Pattern.compile("\"name\":\"(.*?)\",\"year\":\"(.*?)\",\"total\":\"(.*?)\",\"id\":(.*?),\"pic\":\"(.*?)\"");
		
		Matcher m = p.matcher(all);
		ArrayList<Main.subDetails> matches = new ArrayList<Main.subDetails>();
		while(m.find()){
			
			Main.subDetails r = new Main.subDetails();
			r.name = m.group(1);
			r.year = m.group(2);
			r.total = m.group(3);
			r.id = m.group(4);
			r.pic = m.group(5);
			r.link = "http://www.opensubtitles.org/en/search/sublanguageid-eng/idmovie-".concat(r.id);
			
		    matches.add(r);
		    
		    System.out.println(r);
		}
		
	}
	
	
	public void mainSub(String url) throws Exception
	{
		extractListFromPage(url);
		System.out.println(selectedMovieExtractedSubList.size()+" total subtitles found");

		
		HashSet<Main.subDetails> s = new HashSet<Main.subDetails>(selectedMovieExtractedSubList);
		
		System.out.println(s.size()+" after removing duplicates");

		Downloader.downloadAllSubs(new LinkedList<Main.subDetails>(s));
	}
	
	static LinkedList<Main.subDetails> selectedMovieExtractedSubList;
	
	static LinkedList<Main.subDetails> extractListFromPage( String url) throws Exception
	{
		String all = Downloader.getStringFromURL(url);
        Pattern p = Pattern.compile("href=\"/en/subtitles/([0-9]+).*?>(.*?)</a>");
        
		Matcher m = p.matcher(all);
		LinkedList<Main.subDetails> matches = new LinkedList<Main.subDetails>();
		System.out.println("**********************************************");
		while(m.find()){
			Main.subDetails s= new Main.subDetails();
			s.link = "http://dl.opensubtitles.org/en/download/sub/".concat(m.group(1));
			s.link2 = "http://dl.opensubtitles.org/en/download/sub/".concat(m.group(1));
			s.name = m.group(2).trim().replaceAll("[^\\p{L}\\p{Z}]","");
		    matches.add( s);
//		    System.out.println(m.group(0));
//		    System.out.println(s.link);
//		    System.out.println(s.name);
		}
		
		
//		System.out.println(matches.size()+" total subtitles found");
//
//		for (Iterator<Main.subDetails> iterator = matches.iterator(); iterator.hasNext();) {
//		    String string = iterator.next().link;
//		    if (!string.contains("/english/")) {
//		        iterator.remove();
//		    }
//		}
//		
//		System.out.println(matches.size()+" english subtitles found");
		
		HashSet<Main.subDetails> s = new HashSet<Main.subDetails>(matches);
		
		System.out.println(s.size()+" after removing duplicates");
		matches = new LinkedList<Main.subDetails>(s);
		
		selectedMovieExtractedSubList = matches;
		return matches;
		}
}