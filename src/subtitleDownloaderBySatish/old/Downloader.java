package subtitleDownloaderBySatish.old;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Downloader {
	static File getJarFolder() throws SecurityException, URISyntaxException {
		Path u = Paths.get(new Object() {
		}.getClass().getEnclosingClass().getProtectionDomain().getCodeSource().getLocation().toURI());
		return u.getParent().toFile();
	}

	static Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("www-proxy.idc.oracle.com", 80));

	static URLConnection getURLConnection(String urlString) throws IOException {
		URLConnection uc;
		URL url = new URL(urlString);
		if (GUI.orcle != null && GUI.orcle.isSelected())
			uc = (HttpURLConnection) url.openConnection(proxy);
		else
			uc = (HttpURLConnection) url.openConnection();

		uc.addRequestProperty("User-Agent", "Mozilla/4.0");
		return uc;
	}

	static void downloadThread(String dir, LinkedList<Main.subDetails> list, int i) throws IOException {

		System.out.println(i + " thread for download started");
		OutputStream output = null;
		String name = list.get(i).name; // .replaceAll("[^\\p{L}\\p{Z}]","")
		String fn = dir.concat("\\").concat(String.valueOf(i)).concat(name + "  ").trim();

		System.out.println(fn);
		try {
			System.out.println(list.get(i).link2);

			URLConnection uc = getURLConnection(list.get(i).link2);

			String returnedFileName = uc.getHeaderField("Content-Disposition");
			if (returnedFileName == null)
				return;
			returnedFileName = returnedFileName.replaceAll("[^.]*\\.", "");
			returnedFileName = returnedFileName.replaceAll("\"", "");

			fn = fn.concat(".").concat(returnedFileName);
			System.out.println(fn);
			list.get(i).savedFilePath = fn;

			System.out.println(uc.getContentType());

			InputStream input = uc.getInputStream();

			byte[] buffer = new byte[4096];
			int n = -1;

			output = new FileOutputStream(new File(fn));
			while ((n = input.read(buffer)) != -1) {
				if (n > 0) {
					output.write(buffer, 0, n);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (output != null)
				try {
					output.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}

		list.get(i).Downloaded = true;
		Categorize.categorizeFolder(dir);
	}

	static void downloadAllSubs(LinkedList<Main.subDetails> list) throws InterruptedException {

		String dir = System.getProperty("user.home")
				.concat("\\Desktop\\" + Main.current.getSelectedMovieName().replaceAll("[^\\p{L}\\p{Z}]", "").trim());
		new File(dir).mkdirs();
		System.out.println(dir + "  created");

		ArrayList<Thread> downloadArrayList = new ArrayList<Thread>();

		for (int i = 0; i < list.size(); i++) {
			final int f_i = i;
			final String f_dir = dir;
			final LinkedList<Main.subDetails> f_list = list;
			Runnable r = new Runnable() {
				public void run() {
					try {
						downloadThread(f_dir, f_list, f_i);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

			};
			Thread t = new Thread(r);
			executor.execute(t);
			// t.start();
			downloadArrayList.add(t);
		}

		for (int i = 0; i < downloadArrayList.size(); i++) {
			downloadArrayList.get(i).join();
		}

	}

	static ExecutorService executor = Executors.newFixedThreadPool(10);

	public static String getStringFromURL(String u) throws Exception {
		URLConnection uc = getURLConnection(u);
		uc.setDoOutput(true);
		uc.connect();

		BufferedReader rd = new BufferedReader(new InputStreamReader(uc.getInputStream()));

		String line;
		String all = "";
		while ((line = rd.readLine()) != null) {
			all = all.concat(line);
		}
		if (all.length() < 1) {
			System.out.println("URL : " + u);
			System.out.println("Download length" + uc.getContentLengthLong());
			System.out.println("Content type : " + uc.getContentType());
			System.out.println("Content length:" + all.length());
		}
		return all;
	}

	static void saveToFile(Object o, String filename) throws Exception {
		FileOutputStream fos = new FileOutputStream(filename);
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(o);
		oos.close();
	}

	static Object readFromFile(String filename) {
		try {
			FileInputStream fis = new FileInputStream(filename);
			ObjectInputStream ois = new ObjectInputStream(fis);
			Object o = ois.readObject();
			ois.close();
			return o;
		} catch (Exception e) {
			return null;
		}
	}
}