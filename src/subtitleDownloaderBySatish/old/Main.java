package subtitleDownloaderBySatish.old;

import java.io.Serializable;
import java.util.*;

interface ParticularWebsite {
	void searchSub() throws Exception;

	void mainSub(String url) throws Exception;

	String getSelectedMovieName();
	Main.searchText getSearchTextObj();

	void setSelectedMovieName(String x);
}

class Main {

	public static ParticularWebsite current;

	public static void main(String[] args) throws Exception {
		GUI g = new GUI();
		GUI.textFieldSearch.setText("Everest");
		GUI.openSub.setSelected(true);
		GUI.subScene.setSelected(true);
		// GUI.orcle.setSelected(true);
		g.setVisible(true);

	}

	static class searchText {
		public String searchText;

		searchText(String s) {
			searchText = s.replace(" ", "+");
		}

		public HashMap<String, SearchResults> searchResults;

	}

	static class SearchResults implements Comparable<SearchResults> {
		public SearchResults(String name, String link) {
			this.link = link;
			this.name = name;
		}

		String name;
		String link;
		public ArrayList<Main.subDetails> subDetailList;
		
		public boolean equals(SearchResults x) {
			return link.equals(x.link);
		}

		public int hashcode() {
			return link.hashCode();
		}

		public int compareTo(SearchResults x) {
			return this.name.compareTo(x.name);
		}

	}

	static class subDetails implements Comparable<subDetails>, Serializable {
		String year = "";
		String total = "";
		String id = "";
		String pic = "";
		String link = "";
		String name = "";
		String link2 = "";
		String name2 = "";
		String savedFilePath = "";
		boolean downloadLinkGenerated;
		boolean Downloaded;

		public String toString() {
			return name + " " + year;
		}

		public boolean equals(subDetails x) {
			return link2.equals(x);

		}

		public int hashcode() {
			return link2.hashCode();
		}

		public int compareTo(subDetails x) {
			return this.name.compareTo(x.name);
		}

	}
}