package subtitleDownloaderBySatish.MainSearch;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import subtitleDownloaderBySatish.old.Downloader;

public class SearchPage {

	String searchStringURL = "https://subscene.com/subtitles/title?q=".concat("queen");
	String webText;
	ArrayList<WebPage> pages;

	ArrayList<WebPage> getPages() {
		if (pages == null)
			pages = createPages();
		return pages;
	}

	ArrayList<WebPage> createPages() {
		pages = new ArrayList<WebPage>();
		try {
			webText = Downloader.getStringFromURL(searchStringURL);
		} catch (Exception e) {
			e.printStackTrace();
		}

		Pattern p = Pattern.compile("<a href=\"(/sub.*?)\">(.*?)</a>");

		Matcher m = p.matcher(webText);
		while (m.find()) {
			pages.add(new WebPage(m.group(2), "https://subscene.com".concat(m.group(1))));
		}
		return pages;
	}
}
