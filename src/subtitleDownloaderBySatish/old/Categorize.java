package subtitleDownloaderBySatish.old;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

class Categorize {
	public static void main(String args[]) {
		//moveSingle("C:\\Users\\satigupt\\Desktop\\Friends  First Season\\1Friends.S01E12.720p.BluRay.x264-PSYCHD.zip");
		
		String directory = "C:\\Users\\satigupt\\Desktop\\Dexter  Third Season";
		try {
			categorizeFolder(directory);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
//		String file = "C:\\Users\\satigupt\\Desktop\\Dexter  Second Season\\New folder\\1Dexter S02 Complete - BRRip - x264 - AC3 5.1 -={SPARROW}=-.zip";
//		try {
//			unzipFile(file);
//		} catch (IOException e) {
//		}
		
	}
	
	
	static int countFilesInZip(String filePath) {
		int count = 0;
		FileInputStream fis = null;
		ZipInputStream zipIs = null;
		ZipEntry zEntry = null;
		try {
			fis = new FileInputStream(filePath);
			zipIs = new ZipInputStream(new BufferedInputStream(fis));
			while ((zEntry = zipIs.getNextEntry()) != null) {
				count++;
			}
		} finally {
			try {
				fis.close();
				zipIs.close();
			} catch (IOException e) {
				//e.printStackTrace();
			}
			
			return count;
		}
	}
	
	public static boolean unzipFile(String filePath)  {
		FileInputStream fis = null;
		ZipInputStream zipIs = null;
		ZipEntry zEntry = null;
		try {
			fis = new FileInputStream(filePath);
			zipIs = new ZipInputStream(new BufferedInputStream(fis));
			while ((zEntry = zipIs.getNextEntry()) != null) {
				try {
					byte[] tmp = new byte[4 * 1024];
					FileOutputStream fos = null;
					String opFilePath = new File(
							new File(filePath).getParent(), zEntry.getName())
							.toString();
					fos = new FileOutputStream(opFilePath);
					int size = 0;
					while ((size = zipIs.read(tmp)) != -1) {
						fos.write(tmp, 0, size);
					}
					fos.flush();
					fos.close();
				} catch (Exception ex) {
					//ex.printStackTrace();
					continue;
				}
			}

			return true;
		} catch (Exception e) {
			//e.printStackTrace();
			return false;
		} finally {
			try {
				zipIs.close();
				fis.close();
			} catch (IOException e) {}
			
		}
		
	}
	
	static void moveToDone(String filePath){
		File currentFile = new File(filePath);
		File currentFolder = currentFile.getParentFile();
		File doneFolder = new File(currentFolder, "DONE");
		
		

		doneFolder.mkdir();
		try{
		Files.move(currentFile.toPath(), new File(doneFolder, currentFile.getName()).toPath(), StandardCopyOption.REPLACE_EXISTING);
		} catch(IOException e){}
	}
	
	static void categorizeFolder(String folder) throws IOException{
		categorizeFolder1(folder);
		extractExtraLeftFiles(folder);
		categorizeFolder1(folder);
	}
	
	static void extractExtraLeftFiles(String folder) throws IOException{
		File[] files = new File(folder).listFiles();
		for(File file : files){
			if(!file.isFile()) continue;
		  if(file.toString().indexOf(".zip") > -1){
			  if(unzipFile(file.toString()));
			  moveToDone(file.toString());
		  }
		}
	}
	
	static void categorizeFolder1(String folder) throws IOException	{
		File[] files = new File(folder).listFiles();
		for(File file : files){
			if(!file.isFile()) continue;
		  if(countFilesInZip(file.toString()) <4){
		    moveSingle(file.getAbsolutePath());
		  } else if(file.toString().indexOf(".zip") > -1){
			  System.out.println(file);
			  unzipFile(file.toString());
			  moveToDone(file.toString());
		  }
		}
	}

	static public void moveSingle(String path) throws IOException {

		String pattern = "(s|S)(([0-9]{2}).*?((E|e)([0-9]{2})))";
		Pattern r = Pattern.compile(pattern);
		Matcher m = r.matcher(path);
		String categoryFolder="--";

		if (m.find()) {
			categoryFolder = getFormatedString(m.group(3), m.group(6));
		} else {
			r = Pattern.compile("([0-9]{1,2})(x|X)([0-9]{1,2})");
			m = r.matcher(path);
			if (m.find()) {
				categoryFolder = getFormatedString(m.group(1), m.group(3));
			} else
				return;
		}
		
		//System.out.println(categoryFolder);

		File folder = new File(path).getParentFile();
		String filename = new File(path).getName();
		File newFolder = new File(folder,categoryFolder);
		newFolder.mkdirs();
		new File(path).renameTo(new File(newFolder,filename));
		if(new File(newFolder,filename).getAbsolutePath().indexOf(".zip") > -1){
			if(unzipFile(new File(newFolder,filename).getAbsolutePath()));
			moveToDone(new File(newFolder,filename).getAbsolutePath());
		}

	}
	
	private static String getFormatedString(String season, String episode) {
		return "S"
				+ String.format("%02d", Integer.parseInt(season))
				+ "E"
				+ String.format("%02d", Integer.parseInt(episode));
	}

}