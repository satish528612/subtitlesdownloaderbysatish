package subtitleDownloaderBySatish.old;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

class GUI extends JFrame {

	private static final long serialVersionUID = 1L;
	static ArrayList<Main.subDetails> listOfSearched;
	static JFileChooser chooser;

	GUI() {
		setTitle("Subtitle Downloader by Satish");
		setVisible(true);
		this.setSize(800, 600);
		// setBounds(100, 100, 450, 300);
		Container contentPane = getContentPane();
		contentPane.setLayout(new GridBagLayout());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		textFieldSearch = new JTextField(20);
		textFieldSearch.setPreferredSize(new Dimension(200, 20));
		textFieldSearch.setMinimumSize(new Dimension(100, 20));

		GridBagConstraints gBC = new GridBagConstraints();

		JButton buttonFind = new JButton("Find");

		buttonFind.addActionListener(new Find());
		buttonFind.setMnemonic(KeyEvent.VK_F);

		JButton download = new JButton("Download");
		download.setMnemonic(KeyEvent.VK_D);

		download.addActionListener(new Select());

		ButtonGroup group = new ButtonGroup();

		group.add(openSub);
		group.add(subScene);
		subScene.setSelected(true);

		gBC.gridx = 0;
		gBC.gridy = 0;
		gBC.weightx = 10.5;
		contentPane.add(openSub);

		gBC.gridx = 2;
		gBC.gridy = 0;
		contentPane.add(subScene);

		gBC.gridx = 0;
		gBC.gridy = 2;
		contentPane.add(textFieldSearch, gBC);

		gBC.gridx = 0;
		gBC.gridy = 4;
		contentPane.add(buttonFind, gBC);
		gBC.gridx = 4;
		gBC.gridy = 2;

		JScrollPane jScrollPane = new JScrollPane(listbox);
		jScrollPane.setMinimumSize(new Dimension(200, 100));
		jScrollPane.setPreferredSize(new Dimension(300, 400));

		contentPane.add(jScrollPane, gBC);
		gBC.gridx = 4;
		gBC.gridy = 4;
		contentPane.add(download, gBC);

		orcle = new JCheckBox("Don't check it");
		contentPane.add(orcle);

		chooser = new JFileChooser();
		chooser.setCurrentDirectory(new File(System.getProperty("user.home")
				+ "\\Desktop"));
		chooser.setDialogTitle("Select downloaded subtitles folder");
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		//
		// disable the "All files" option.
		//
		chooser.setAcceptAllFileFilterUsed(false);
		//

		JButton buttonCategorize = new JButton(new AbstractAction() {

			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent ae) {
				if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
					System.out.println("Folder to categorize "
							+ chooser.getSelectedFile());
					try {
						Categorize.categorizeFolder(chooser.getSelectedFile()
								.toString());
					} catch (IOException e) {
						e.printStackTrace();
					}
				} else {
					System.out.println("No Selection ");
				}
			}
		});
		buttonCategorize.setText("Cateogize");
		gBC.gridx = 0;
		gBC.gridy = 6;
		contentPane.add(buttonCategorize, gBC);

		// contentPane.add(chooser);

	}

	static JTextField textFieldSearch;
	static DefaultListModel<String> listModel = new DefaultListModel<String>();
	static JList<String> listbox = new JList<String>(listModel);
	static JRadioButton openSub = new JRadioButton("opensubtitle.org");
	static JRadioButton subScene = new JRadioButton("Subscene.com");
	static JCheckBox orcle;

}

class Select extends AbstractAction {

	private static final long serialVersionUID = 1L;

	public void actionPerformed(ActionEvent ev) {
		System.out.println("Download clicked");

		HashMap<String, Main.SearchResults> x =  Main.current.getSearchTextObj().searchResults;

		System.out.println(x.get(GUI.listbox.getSelectedValue()));

		String url = x.get(GUI.listbox.getSelectedValue()).link;
		System.out.println(url);
		try {
			Main.current.setSelectedMovieName(Main.current.getSearchTextObj().searchResults.get(
					GUI.listbox.getSelectedValue()).name);
			Main.current.mainSub(url);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

class Find extends AbstractAction {

	private static final long serialVersionUID = -2590438972170266690L;

	public void actionPerformed(ActionEvent ev) {
		String searchText = GUI.textFieldSearch.getText();

		if (GUI.subScene.isSelected())
			Main.current = new SubScene(searchText);
		else
			Main.current = new OpenSub(searchText);

		System.out.println("Find clicked for - " + searchText);
		try {
			Main.current.searchSub();
			GUI.listModel.removeAllElements();
			
			Iterator it = Main.current.getSearchTextObj().searchResults.entrySet().iterator();
		    while (it.hasNext()) {
		        Map.Entry pair = (Map.Entry)it.next();
		        System.out.println(pair.getKey() + " = " + pair.getValue());
		        GUI.listModel.addElement(pair.getKey().toString());
		        }
		    
		} catch (Exception e) {
			System.out.println("Exception occured in Find of GUI");
			e.printStackTrace();
		}
	}
}